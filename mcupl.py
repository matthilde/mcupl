#!/usr/bin/env python3

# == TODO TODO TODO TODO TODO ==
# 
# [x] Expression Parsing w/ AST
#   [ ] Maybe add lambdas
# [x] Function defining
#   [x] Pattern Matching
#   [x] Guards
# [x] Function execution
#   [x] Multiple definitions on the same name
#   [ ] (?) Lambda
# [ ] stdlib
#   [x] TTY I/O (print, input)
#   [ ] File I/O
#   [ ] Array and string manipulation
#   [ ] (?) Network
# [x] Interpreter
#   [x] Read from file
#   [x] REPL w/ readline
# [ ] IMPORTANT: Code cleanup

from dataclasses import dataclass, field
import sys, re, operator, string

# a = b
# a = [b, c, d, e]
# f.x = b
# f.x y = b
# f.x _ = b
# f._ _ = b
# f.[a|b] = c
# f.[a|_] = b
# f.[_|a] = b

# Arity -> int for numbers, str for the rest

###### USEFUL DATA

_kwlist = lambda keywords: [('keyword', x) for x in keywords]
# Keywords by priority
_prior1 = _kwlist(['||', '&&'])
_prior2 = _kwlist(['==', '!=', '>=', '<=', '>', '<'])
_prior3 = _kwlist(['+',  '-', '++'])
_prior4 = _kwlist(['*', '/', '%'])
_defkeywords = ['.', '?']
_patkeywords = ['[', ']', '|', ',']
_keywords = [
        ':',
        "+", "/", "*", "-", "%",
        "&&", "||", "!", "==", "!=", ">", "<", "<=", ">=",
        "(", ")", ",", "[", "]"
        ]
# "Forbidden" characters in definition names for the sake of the lexer
_fchars = ".?[], +/|*-%&:=!<>()"
_keywordops = {
        '+': operator.add,
        '/': operator.floordiv,
        '*': operator.mul,
        '-': operator.sub,
        '%': operator.mod,
        '&&': lambda a, b: operator.and_(bool(a), bool(b)),
        '||': lambda a, b: operator.or_(bool(a), bool(b)),
        '!':  lambda a:    operator.not_(bool(a)),
        '==': operator.eq,
        '!=': operator.ne,
        '>':  operator.gt,
        '<':  operator.lt,
        '<=': operator.le,
        '>=': operator.ge
        }

_escapes = {
        'a': '\a',
        'b': '\b',
        'f': '\f',
        'n': '\n',
        'r': '\r',
        't': '\t',
        'v': '\v',
        "'": '\'',
        '"': '\"',
        '\\': '\\',
        '0': '\0'
        }


###############################################################################

# To make difference between local and global scope
@dataclass
class MScope:
    glob: dict            # Since dicts are pointers, it will just point
                            # to the dict I specify in the class.
    local: dict = field(default_factory = dict)

    def __getitem__(self, name):
        if name in self.glob:
            return self.glob[name]
        elif name in self.local:
            return self.local[name]
        else:
            raise KeyError(f"'{name}'")
    
    def __iter__(self):
        yield from self.glob
        yield from self.local

    def __setitem__(self, k, v):
        self.local[k] = v

    # If you want to add to global scope, do scope.glob[k] = v

class MExpression:
    def __init__(self, expr, raw = True):
        if raw:
            assert type(expr) == str, "expr must be a string."

            self.raw_expr = expr
            self.expr     = parse_expr(expr)
        else:
            self.raw_expr = None
            self.expr     = expr

    def __call__(self, scope: dict):
        return self.expr.get(scope)

@dataclass
class MFunction:
    expr:   MExpression
    lscope: dict

    def __call__(self, scope, lscope = {}):
        if type(scope) == MScope:
            scope = scope.glob
        
        local_scope = {**self.lscope, **lscope}
        scope = MScope(glob = scope, local = local_scope)

        return self.expr(scope)

# Holds multiple functions
@dataclass
class MFunctions:
    defs: list

    __setitem__ = lambda self, *kargs: self.defs.__setitem__(*kargs)
    __getitem__ = lambda self, *kargs: self.defs.__getitem__(*kargs)
    __item__    = lambda self, *kargs: self.defs.__item__(*kargs)

    def append(self, definition):
        assert type(definition) == MFunctionDef, "I think the code is fucked up tbh"
        self.defs.append(definition)
    
    def __call__(self, scope, args: list):
        assert (f:=self.get_fn(scope, args)) != None, "Undefined function."
        return f(scope)

    def get_fn(self, scope, args: list):
        def pattern_match(x, y):
            if x == '_':
                return True
            elif x[0] == 'n' and x[1] == y:
                return True
            elif x[0] == 'v':
                lscope[x[1]] = y
                return True
            else:
                return False

        for fn in self.defs:
            if len(args) != len(fn.args):
                continue
            
            # Checking the argument
            success = True
            lscope = {}
            for x, y  in zip(fn.args, args):
                if x[0] == 'l':
                    if type(y) != list or len(y) != len(x[1]):
                        success = False
                        break
                    elif False in [pattern_match(xx, yy) 
                                    for xx, yy in zip(x[1], y)]:
                        success = False
                        break
                elif x[0] == 's':
                    if type(y) != list or len(y) < len(x[1].head) + 1:
                        success = False
                        break
                    
                    head = y[:len(x[1].head)]
                    tail = y[len(x[1].head):]
                    if not pattern_match(x[1].tail, tail):
                        success = False
                        break

                    if False in [pattern_match(xx, yy)
                                    for xx, yy in zip(x[1].head, head)]:
                        success = False
                        break
                elif not pattern_match(x, y):
                    success = False
                    break
            
            if success and (fn.guard == None or fn.guard(MScope(scope, lscope))):
                    return MFunction(expr = fn.expr, lscope = lscope)
        
        return None


###############################################################################

###### AST TYPES

@dataclass
class MList:
    value: list
    
    def __getitem__(self, item):
        return self.value[item]
    
    def __add__(self, h):
        if type(h) == list:
            self.value += [MVariable(value = x) for x in h]
        else:
            self.value += h.value

    def get(self, scope: dict):
        out = []
        for v in self.value:
            out.append(v.get(scope))
        return out

@dataclass
class MOperand:
    operator: str
    a:        'typing.Any'
    b:        'typing.Any'

    def get(self, scope: dict):
        if self.operator == '!':
            a = self.a.get(scope)
            assert type(a) == int, "! keyword only takes integers."
            return int(_keywordops['!'](a))
        else:
            a = self.a.get(scope)
            b = self.b.get(scope)
            assert type(a) == type(b), "operands are not in the same type."
            c = _keywordops[self.operator](self.a.get(scope), self.b.get(scope))
            assert type(c) in [int, bool] or self.operator == '+', \
                    f"{self.operator} only takes integers."
            return int(c) if type(c) in [int, bool] else c

@dataclass
class MVariable:
    value: 'typing.Any'

    def get(self, scope: dict):
        if type(self.value) == int:
            return self.value
        elif self.value in scope:
            v = scope[self.value]
            assert type(v) in [int, list] or callable(v), "Invalid variable type."
            return v
        else:
            raise AssertionError("Value not in scope.")

@dataclass
class MASTFunction:
    name: str
    args: list

    def get(self, scope: dict):
        assert self.name in scope, f"Function '{self.name}' not in scope."
        assert callable(scope[self.name]), "{self.name}: Expected a function"

        try:
            ret = scope[self.name](scope, [x.get(scope) for x in self.args])
        except AssertionError as e:
            raise AssertionError(f"{self.name}: {e}")

        if ret == None:
            return 0
        elif type(ret) == float:
            return int(ret)
        elif type(ret) == str:
            return [ord(x) for x in ret]
        else:
            return ret

# Imperative
@dataclass
class MImperative:
    exprs: list

    def get(self, scope: dict):
        scope = MScope(scope)

        for expr in self.exprs:
            ret = expr.get(scope)
        return ret

# Array pattern
@dataclass
class MArrayPattern:
    head: list
    tail: tuple

# Function definition
@dataclass
class MFunctionDef:
    name:  str
    args:  list
    guard: MExpression
    expr:  MExpression



###############################################################################

# Making a "main" function so I don't have to rewrite the function over and over
def _tokenize(s, keywords, do_strings = True, do_spaces = True, stopper = '#', \
                           must_use_stopper = False, give_rest = False):
    escapes  = _escapes

    out = []
    cur = ''
    i   = 0

    while i < len(s):
        c = s[i]
        if c in " \t\n" and do_spaces:
            if cur:
                out.append(('value', cur))
                cur = ''
        elif s[i:i+len(stopper)] == stopper:
            must_use_stopper = False
            i += len(stopper)
            break
        elif c == '"' and do_strings:
            cur = ''
            i += 1
            while i < len(s) and s[i] != '"':
                c = s[i]
                if c == '\\':
                    i += 1
                    assert s[i] in escapes, "Invalid escape sequence."
                    cur += escapes[s[i]]
                else:
                    cur += c
                i += 1
            
            out.append(('string', cur))
            cur = ''
        elif s[i:i+2] in keywords:
            if cur:
                out.append(('value', cur))
                cur = ''
            out.append(('keyword', s[i:i+2]))
            i += 1
        elif c in keywords:
            if cur:
                out.append(('value', cur))
                cur = ''
            out.append(('keyword', c))
        else:
            cur += c

        i += 1

    assert not must_use_stopper, f"'{stopper}' not found"

    if cur:       out.append(('value', cur))
    if give_rest: out.append(('rest', s[i:]))
    return [(x, y.strip() if x != 'string' else y) for x, y in out]

# Parse pattern for pattern matching
def pp_tokenize(s):
    return _tokenize(s, _patkeywords, False)

def pp_parse(tokens, is_list = False):
    # fuck it
    getval = lambda x: '_' if x=='_'      else ('n', int(x)) \
                           if x.isdigit() else ('v', x)

    if is_list:
        assert tokens.count(('keyword', '|')) <= 1, \
                "Too many slices (expected one or less)"
        
        if (is_slice := ('keyword', '|') in tokens):
            assert len(tokens) >= 3, "Syntax Error"
            assert tokens[-2] == ('keyword', '|'), "mu"
            assert tokens[-1][0] == 'value', " m u "

            tail = getval(tokens[-1][1])
            tokens = tokens[:-2]

        head = []
        tmp = []
        while tokens != []:
            n = tokens.pop(0)
            if (n[0] == 'value'):
                tmp.append(n[1])
            elif n == ('keyword', ',') and len(tmp) == 1:
                head.append(getval(tmp[0]))
                tmp = []
                continue
            else:
                raise SyntaxError("Syntax Error.")
        if len(tmp) == 1:
            head.append(getval(tmp[0]))
        elif len(tmp) > 1:
            raise SyntaxError("Syntax Error.")

        return ('s', MArrayPattern(head=head, tail=tail)) if is_slice else ('l', head)
    else:
        result = []
        while tokens != []:
            n = tokens.pop(0)
            if n == ('keyword', '['):
                tmp = []
                while (n:=tokens.pop(0)) != ('keyword', ']'):
                    assert n != ('keyword', '['), "Misplaced bracket."
                    tmp.append(n)
                result.append(pp_parse(tmp, True))
                tmp = []
            elif n[0] == 'value':
                result.append(getval(n[1]))
            else:
                raise SyntaxError("Syntax Error.")

        return result

def parse_pattern(s):
    tokens = pp_tokenize(s)
    assert tokens.count(('keyword', '[')) == tokens.count(('keyword', ']')), \
            "Missing brackets."
   
    return pp_parse(tokens)

# Parse function definition
def pf_tokenize(s):
    return _tokenize(s, _defkeywords, False, False, ':=', True, True)

def parse_function_def(s):
    tokens = pf_tokenize(s)
    
    assert len(tokens) >= 2, "Invalid expression."
    assert tokens[-1][0] == 'rest' and tokens[-1][1] != '', \
            "Expression is missing."
    assert tokens[0][0] == 'value', "Missing function name."
    fname = tokens.pop(0)[1]        # name
    assert all(map(lambda c: c not in _fchars, fname)), "Invalid definition name."
    
    n = tokens.pop(0)               # .
    if n[0] == 'rest':
        return MFunctionDef(fname, None, None, MExpression(n[1]))
    elif n != ('keyword', '.'):
        raise SyntaxError("Syntax Error.")

    n = tokens.pop(0)               # x y
    if n == ('keyword', '?'):
        fargs = []
    elif n[0] == 'value':
        fargs = parse_pattern(n[1])
        n = tokens.pop(0)
    else:
        raise SyntaxError("Syntax Error.")
    
    fguard = None                   # ?
    if n == ('keyword', '?'):
        fguard = MExpression(tokens.pop(0)[1])
        n = tokens.pop(0)
    elif n[0] != 'rest':
        raise SyntaxError("Syntax Error.")
    
    fexpr = n
    assert fexpr[0] == 'rest', "Syntax Error."
    fexpr = MExpression(fexpr[1])

    return MFunctionDef(fname, fargs, fguard, fexpr)
    
# arr.find with error handling and some assertions
def _xindex(arr, elements):
    b = 0
    # Little hack to prevent from looking into the parantheses :>
    arr = arr.copy()
    for i, x in enumerate(arr):
        if x == ('keyword', '(') or x == ('keyword', '['):
            b += 1
        elif x == ('keyword', ')') or x == ('keyword', ']'):
            b -= 1

        if b > 0:
            arr[i] = None

    for element in elements:
        if element in arr:
            n = arr.index(element)
            assert n != 0, "Wrongly placed operand. smh place them properly"
            return n
    return -1

# abc... + / * - % ++ && || ( ) , [ ] "
def pe_tokenize(s):
    return _tokenize(s, _keywords)

# Time to parse the AST
def pe_ast(tokens):
    assert tokens != [], "Syntax Error."

    n = 0 # Why do I even init the variable :v
    
    if ('keyword', ':') in tokens:
        exprs = []
        tmp   = []
        for token in tokens:
            if token == ('keyword', ':') and tmp != []:
                exprs.append(pe_ast(tmp))
                tmp = []
            elif token == ('keyword', ':'):
                raise SyntaxError("Syntax Error.")
            else:
                tmp.append(token)

        if tmp != []:
            exprs.append(pe_ast(tmp))
        return MImperative(exprs)

    ### Operands
    if (n := _xindex(tokens, _prior1)) != -1:
        return MOperand(tokens[n][1], pe_ast(tokens[:n]), pe_ast(tokens[n+1:]))
    elif (n := _xindex(tokens, _prior2)) != -1:
        return MOperand(tokens[n][1], pe_ast(tokens[:n]), pe_ast(tokens[n+1:]))
    elif (n := _xindex(tokens, _prior3)) != -1:
        return MOperand(tokens[n][1], pe_ast(tokens[:n]), pe_ast(tokens[n+1:]))
    elif (n := _xindex(tokens, _prior4)) != -1:
        return MOperand(tokens[n][1], pe_ast(tokens[:n]), pe_ast(tokens[n+1:]))
    
    ### Detecting the not gate
    nothere = False
    if tokens[0] == ('keyword', '!'):
        nothere = True
        tokens = tokens[1:]

    ### Parentheses, lists, and values
    if len(tokens) == 1 and tokens[0][0] == 'value':
        val = tokens[0][1]
        if val.isdigit():
            h = MVariable(int(val))
        else:
            h = MVariable(val)
    elif tokens[0] == ('keyword', '(') and tokens[-1] == ('keyword', ')'):
        h = pe_ast(tokens[1:-1])
    elif tokens[0][0] == 'value' and (not tokens[0][0].isdigit()) and \
         tokens[1]    == ('keyword', '(') and \
         tokens[-1]   == ('keyword', ')'):
        # Parsing all the ,
        if len(tokens) == 3:
            h = MASTFunction(name = tokens[0][1], args = [])
        else:
            args = []
            temp = []
            b = 0
            for token in tokens[2:-1]:
                if token == ('keyword', '[') or token == ('keyword', '('):
                    b += 1
                elif token == ('keyword', ']') or token == ('keyword', ')'):
                    b -= 1
                assert b >= 0, "Misplaced brackets."

                if token == ('keyword', ',') and b == 0:
                    args.append(pe_ast(temp))
                    temp = []
                else:
                    temp.append(token)
            args.append(pe_ast(temp))
            h = MASTFunction(name = tokens[0][1], args = args)
    elif tokens[0] == ('keyword', '[') and tokens[-1] == ('keyword', ']'):
        if len(tokens) == 2:
            return MList([])
        args = []
        temp = []
        for token in tokens[1:-1]:
            if token == ('keyword', ','):
                args.append(pe_ast(temp))
                temp = []
            else:
                temp.append(token)
        args.append(pe_ast(temp))
        return MList(args)
    elif tokens[0][0] == 'string':
        return MList([MVariable(ord(x)) for x in tokens[0][1]])
    else:
        raise SyntaxError("Syntax Error.")

    return MOperand('!', h, 0) if nothere else h

###############################################################################

# Must be a line of code
def parse_line(s):
    return parse_expr(s)

def parse_expr(s):
    tokens = pe_tokenize(s)
    if tokens == []:
        return MVariable(0)
    assert tokens.count(('keyword', '(')) == tokens.count(('keyword', ')')), \
            "Missing parentheses"
    assert tokens.count(('keyword', '[')) == tokens.count(('keyword', ']')), \
            "Missing brackets"
    return pe_ast(tokens)

def parse_arity(self, arity):
    pass

###############################################################################

### THE EPIC GAMER STDLIB FOR MCUPL BY MATTHILDE

_printable = lambda n: False not in [chr(x) in string.printable for x in n]
_ltos = lambda x: ''.join([chr(i) for i in x])
_vtos = lambda x: _ltos(x) if type(x) == list and _printable(x) else \
                  '<function>' if callable(x) else x

### TTY I/O

def _mprint(scope, args):
    if len(args) == 0:
        print()
    elif len(args) == 1:
        print(_ltos(args[0]))
    else:
        try:
            print(_ltos(args[0]).format(*[_vtos(x) for x in args[1:]]))
        except IndexError:
            raise SyntaxError("Format error.")

    return 0

def _minput(scope, args):
    prompt = "" if len(args) == 0 else _ltos(args[0])
    return [ord(x) for x in input(prompt)]

### File I/O

def _open(scope, args):
    _ol = {'r': os.O_RDONLY, 'w': os.O_WRONLY, 'rw': os.O_RDWR}
    assert len(args) == 2, "Invalid arity"
    assert type(args[0]) == list and type(args[1]) == int, "Invalid arguments"

    try:
        return os.open(_ltos(args[0]), args[1])
    except FileNotFoundError:
        raise AssertionError("File not found.")
    except PermissionError:
        raise AssertionError("Unable to open file.")

def _close(scope, args):
    assert len(args) == 1 and type(args[0]) == int, "Invalid arguments"
    
    try:
        os.close(args[0])
    except:
        raise AssertionError("Unable to close file.")

def _read(scope, args):
    assert 3 >= len(args) >= 2, "Invalid arity"

    raise AssertionError("Not implemented yet.")

#_readword
#_readline
#_write

### LIST MANIPULATION

#_nth
#_strip
#_lstrip
#_rstrip
#_split
#_sub

### MISC

#_typeof

stdlib = {
    "print": _mprint, "input": _minput
}

###############################################################################

def repl():
    print("MCUPL Console by matthilde")
    print("NOTE: This is a learning project, avoid using this programming")
    print("      language for actual projects.")
    print("Type '.exit' to leave the interpreter\n")

    scope = stdlib
    commands = {
        "exit": (sys.exit, 0),
        "clear": (lambda: os.system('clear'), 0),
        "del": (lambda x: scope.__delitem__(x) if x in scope else None, 1)
    }

    while True:
        try:
            l = input("\033[32;1m IN: \033[0m").strip()
            
            if l == '' or l.startswith("#"):
                continue
            elif l.startswith("."):
                command = l[1:].split()
                assert command[0] in commands, "Interpreter command not found."
                
                cmd = commands[command[0]]
                assert cmd[1] == len(command[1:]), "Invalid command usage."

                try:
                    cmd[0](*command[1:])
                except Exception as e:
                    print(f"{type(e).__name__}: {e}", file=sys.stderr)

            elif ('keyword', ':=') in _tokenize(l, [':=']):
                ndef = parse_function_def(l)
                if ndef.args == ndef.guard == None:
                    scope[ndef.name] = ndef.expr(scope)
                elif ndef.name not in scope:
                    scope[ndef.name] = MFunctions([ndef])
                else:
                    scope[ndef.name].append(ndef)
            else:
                expr = MExpression(l)
                ret  = expr(scope)

                print("\033[31;1mOUT:\033[0m ", end='')
                if callable(ret):
                    print("<function>")
                elif type(ret) == list and _printable(ret):
                    print(repr(''.join([chr(x) for x in ret])))
                else:
                    print(ret)
        except KeyboardInterrupt:
            print()
            continue
        except SyntaxError as e:
            print("ERROR: Syntax Error.", file=sys.stderr)
        except AssertionError as e:
            print("ERROR:", e, file=sys.stderr)

def error_at(lineno, line, err):
    print("ERROR AT LINE", lineno, file=sys.stderr)
    print("  ", err, file=sys.stderr, end='\n\n')
    print(">", line, file=sys.stderr)


def read_program(scope, fn):
    lines = []
    with open(fn, 'r') as f:
        lines = [x.strip() for x in f.readlines() if \
                 x.strip() != '' and not x.strip().startswith('#')]
    
    dlocations = {}
    for lineno, line in enumerate(lines):
        lineno += 1
        try:
            ndef = parse_function_def(line)
            if ndef.args == ndef.guard == None:
                scope[ndef.name] = ndef.expr(scope)
            elif ndef.name not in scope:
                scope[ndef.name] = MFunctions([ndef])
            else:
                scope[ndef.name].append(ndef)

            dlocations[ndef.name] = (lineno, line)
        except AssertionError as e:
            error_at(lineno, line, e)
            return None
        except SyntaxError as e:
            error_at(lineno, line, e)
            return None
    
    return dlocations

def eval_file(fn, args):
    if not os.path.isfile(fn):
        print("ERROR: File not found.")
        return 1
    scope = stdlib

    if (sd:=read_program(scope, fn)) == None:
        return 1
    
    if "main" not in scope:
        print("ERROR: No main function.", file=sys.stderr)
        return 1

    try:
        try:
            scope["main"](scope, args)
        except AssertionError as e:
            raise AssertionError(f"main: {e}")
    except AssertionError as e:
        eep = str(e).split(':')
        if len(eep) == 1 or (len(epp) > 1 and eep[0] not in sd):
            print("ERROR:", e, file=sys.stderr)
        else:
            linenum, line = sd[eep[0]]
            error_at(linenum, line, ':'.join(eep[1:]).strip())

def main(fn = None):
    if not fn:
        repl()
    else:
        return eval_file(fn, sys.argv[2:])
    
    return 0

if __name__ == "__main__":
    import readline, os, string
    ret = main(sys.argv[1] if len(sys.argv) >= 2 else None)
    sys.exit(ret)
